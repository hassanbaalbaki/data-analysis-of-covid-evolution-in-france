
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import getLatestDataSets
import read_covid_data
import pandas as pd
import numpy as np

def draw_curfew_periods(fig):
    #Get the Date for curefews and confinement
    firstCurfew_date= read_covid_data.firstCurfew_date
    secondCurfew_date=read_covid_data.secondCurfew_date
    second_confinement_date= read_covid_data.second_confinement_date
    lite_confinement_date=read_covid_data.lite_confinement_date
    end_confinement_date= read_covid_data.end_confinement_date
    third_curfew_date=read_covid_data.third_curfew_date#15 dep
    forth_curfew_date=read_covid_data.forth_curfew_date #25 dep
    generalised_curfew_date=read_covid_data.generalised_curfew_date
    weekend_confinement_date=read_covid_data.weekend_confinement_date #Alpes-Martimes et Nord
    weekend_deconfinement_date=read_covid_data.possible_weekend_deconfinement_date # date possible

    #highlight the periods with different colors
    fig.add_vrect(x0=firstCurfew_date, x1=secondCurfew_date,fillcolor="LightSalmon",annotation_text="Couvre-feu <br>10 métroples", opacity=0.3,layer="below", line_width=0,)
    fig.add_vrect(x0=secondCurfew_date, x1=second_confinement_date,fillcolor="orange",annotation_text="Couvre-feu <br>54 départements", annotation_position="top left", opacity=0.3,layer="below", line_width=0,)
    fig.add_vrect(x0=second_confinement_date, x1=lite_confinement_date,fillcolor="red",annotation_text="Deuxième <br>confinement", opacity=0.3,layer="below", line_width=0,)    
    fig.add_vrect(x0=lite_confinement_date, x1=end_confinement_date,fillcolor="red",annotation_text="Confinement <br>allégé", opacity=0.2,layer="below", line_width=0,)  
    fig.add_vrect(x0=end_confinement_date, x1=third_curfew_date,exclude_empty_subplots=True,fillcolor="orange",annotation_text="Post confinement<br> Couvre-feu à 20h", opacity=0.2,layer="below", line_width=0,) 
    fig.add_vrect(x0=third_curfew_date, x1=forth_curfew_date,exclude_empty_subplots=True,fillcolor="orange",annotation_text="Couvre-feu à 18h <br> dans 15 dép.", opacity=0.3,layer="below", line_width=0,)
    fig.add_vrect(x0=forth_curfew_date, x1=generalised_curfew_date,exclude_empty_subplots=True,fillcolor="orange",annotation_text="25 dép.", opacity=0.4,layer="below", line_width=0,)
    fig.add_vrect(x0=generalised_curfew_date, x1=weekend_confinement_date,exclude_empty_subplots=True,fillcolor="orange",annotation_text="Couvre-feu <br> généralisé à 18h", opacity=0.5,layer="below", line_width=0,)
    fig.add_vrect(x0=weekend_confinement_date, x1=weekend_deconfinement_date,fillcolor="red",annotation_text="Confinement du week-end,<br> Nice et Dunkerque", opacity=0.2,layer="below", line_width=0,)  
    
    #add lines to seperate the different periods visually
    fig.add_vline(x=firstCurfew_date, line_width=3, line_dash="dot", line_color="orange",)
    fig.add_vline(x=secondCurfew_date, line_width=3, line_dash="dash", line_color="orange",)
    fig.add_vline(x=second_confinement_date, line_width=3, line_dash="dash", line_color="red",)
    fig.add_vline(x=lite_confinement_date, line_width=3, line_dash="dash", line_color="red",)
    fig.add_vline(x=end_confinement_date, line_width=3, line_dash="dash", line_color="orange",)
    fig.add_vline(x=third_curfew_date, line_width=3,exclude_empty_subplots=True, line_dash="dot", line_color="orange",)
    fig.add_vline(x=forth_curfew_date, line_width=3,exclude_empty_subplots=True, line_dash="dot", line_color="orange",)
    fig.add_vline(x=generalised_curfew_date, line_width=3,exclude_empty_subplots=True, line_dash="dot", line_color="orange",)
    fig.add_vline(x=weekend_confinement_date, line_width=3, line_dash="dash", line_color="red",)
    fig.add_vline(x=weekend_deconfinement_date, line_width=3, line_dash="dash", line_color="orange",)
    


app = dash.Dash(__name__)

all_deps = read_covid_data.get_population_df().loc[:,"dep"].values

app.layout = html.Div([
    html.Div(id="page_status"),
    dcc.Dropdown(
        id="selectedDepartements",
        options=[{"label": dep, "value": dep} 
                 for dep in all_deps],
        value=["06","13","38","42","62","69","75","83","93"],
        multi=True
    ),
    html.H2("Incidence des Cas positifs par département, normalisée pour un millon d'habitants et lissée sur une semaine"),
    dcc.Graph(id="pcr_chart"),
    html.H2("Différence sur une semaine des cas positifs par département (%)"),
    dcc.Graph(id="pcr_var_chart"),
    html.H2("Incidence des admission à l'hopital par département pour un millon d'habitants"),
    dcc.Graph(id="inc_hosp_chart"),
    html.H2("Incidence des décés à l'hopital par département pour un millon d'habitants"),
    dcc.Graph(id="inc_dc_chart"),
    html.H2("Nombre de patients hospitalisées par département pour un millon d'habitants"),
    dcc.Graph(id="cur_hosp_chart"),
    html.H2("Nombre de patients en réanimation par département pour un millon d'habitants"),
    dcc.Graph(id="cur_rea_chart"),
    html.H2("Nombre total de décés normalisés pour un millon d'habitants"),
    dcc.Graph(id="acc_dc_chart"),
    html.H2("Nombre total de décés depuis le début de seconde vague, normalisés pour un millon d'habitants"),
    dcc.Graph(id="acc_dc2_chart"),
    html.Button('Mettre à jour les données', id='update_data_btn', n_clicks=0),
])

#Update PCR Charts
@app.callback(
    [Output("pcr_chart", "figure"),
    Output("pcr_var_chart","figure")],  
    [Input("selectedDepartements", "value")])
def update_pcr_charts(deps):
    #Test PRC
    tests_per_million=read_covid_data.get_pcr_tests_data_per_million_filtered_by_departements(deps)
    
    fig_pcr= px.line(tests_per_million,x="jour", y='ppm', color='dep',height=800)
    draw_curfew_periods(fig_pcr)

    fig_pcr_var= px.line(tests_per_million,x="jour", y='pct_SP', color='dep',height=800)
    draw_curfew_periods(fig_pcr_var)
    


    return [fig_pcr,fig_pcr_var]

#Update Incidence Charts
@app.callback(
    [Output("inc_hosp_chart","figure"),
    Output("inc_dc_chart","figure")],  
    [Input("selectedDepartements", "value")])
def update_incidence_charts(deps):

    #Incidence of new hospital admissions and new deaths
    inc_per_million= read_covid_data.get_daily_incidence_covid_metrics_normalised_per_million_filtered_by_departements(deps)
    #Hospital Admissions
   
    fig_inc_hosp=px.line(inc_per_million,x="jour", y='hpm', color='dep' ,height=800)
    draw_curfew_periods(fig_inc_hosp)
    #New Death
    fig_inc_dc=px.line(inc_per_million,x="jour", y='dpm', color='dep',height=800)
    draw_curfew_periods(fig_inc_dc)

    return [fig_inc_hosp,fig_inc_dc]

@app.callback(
    [Output("cur_hosp_chart","figure"),
    Output("cur_rea_chart","figure"),
    Output("acc_dc_chart","figure"),
    Output("acc_dc2_chart","figure")],  
    [Input("selectedDepartements", "value")])
def update_accumulated_charts(deps):

    #Current hospitalisations and total deaths
    acc_metrics_per_million = read_covid_data.get_accumulated_covid_metrics_per_million_filtered_by_departements(deps)
    # Nb of patients hospitalised
    #cur_hosp_df = cur_hosp_per_million.melt(id_vars=["jour"],value_vars=deps,var_name='Departement', value_name='Hospitalisations')
    #fig_cur_hosp=px.line(cur_hosp_df,x="jour", y='Hospitalisations', color='Departement')
    fig_cur_hosp=px.line(acc_metrics_per_million,x="jour",y="hpm",color="dep",height=600)
    draw_curfew_periods(fig_cur_hosp)
     # Nb of Patient in ICU 
    #cur_rea_df = cur_rea_per_million.melt(id_vars=["jour"],value_vars=deps,var_name='Departement', value_name='Reanimations')
    #fig_cur_rea=px.line(cur_rea_df,x="jour", y='Reanimations', color='Departement')
    fig_cur_rea=px.line(acc_metrics_per_million,x="jour",y="rpm",color="dep",height=600)
    draw_curfew_periods(fig_cur_rea)

    #total death
    fig_acc_dc=px.line(acc_metrics_per_million,x="jour",y="dpm",color="dep",height=600)
    draw_curfew_periods(fig_acc_dc)

    #total death since second curfew
    fig_acc_dc2=px.line(acc_metrics_per_million.loc[acc_metrics_per_million["jour"] >= read_covid_data.firstCurfew_date ],x="jour",y="dpm2",color="dep",height=600)
    draw_curfew_periods(fig_acc_dc2)



    return [fig_cur_hosp,fig_cur_rea,fig_acc_dc,fig_acc_dc2]

#Download the latest Dataset
@app.callback(
    Output("page_status","children"),
    Input("update_data_btn","n_clicks"))
def update_data(n_clicks):
    if n_clicks > 0 :
        getLatestDataSets.update_PCR_data()
        getLatestDataSets.update_covid_incidence_data()
        return "La dernière version des données a été téléchargée"
    return

app.run_server(debug=False,host= '0.0.0.0' ,port=8081)
