This project used Python Panda, to analyse the Covid data upload daily on the french santé publique site.
Here is a sample of the charts generated for a predefined set of  departements.

# Sample Charts

- Positive Cases Evolution ( Second Wave) 

![](./fig/PositiveCases.png)

- Hospital Admissions for Covid (Second Wave Only)

![](./fig/Hosp_2emeVg.png)

- Hospital Admissions for Covid (1st and Second Wave)

 ![](./fig/Hospt1stAnd2ndWave.png)




# How to Install and Run the project :

-       Install Anaconda  <https://www.anaconda.com/products/individual#Downloads>
       OR install Python 3
       
-       Clone the projet : 
        `git clone https://gitlab.com/hassanbaalbaki/data-analysis-of-covid-evolution-in-france.git`

-       Install the Required python module : requests ,beautifulsoup4, pandas, plotly and dash 
        Or  `pip install -r requirements-dash.txt`

-       Run the interactive Data app by typing : `python dash-covid-charts.py`

# In order to run the prediction Section 
- Install Darts <https://github.com/unit8co/darts/issues/235> using conda 

        - conda create cov-env python=3.7
        - conda activate cov-env
        - conda install -c conda-forge -c pytorch pip fbprophetpytorch==1.5.1 cpuonly
        - pip install u8darts[all]==0.5.0
