# set base image (host OS)
FROM python:3.7-slim

# set the working directory in the container
RUN mkdir /code

# copy the dependencies file to the working directory
COPY requirements-dash.txt /code

WORKDIR /code

# install dependencies
RUN pip install -r requirements-dash.txt


#Expose the streamlit port
EXPOSE 80

#python/streamlit was not working without this environement 
ENV GIT_PYTHON_REFRESH=quiet

# copy the content of the local src directory to the working directory
COPY data/ data/
COPY dash-covid-charts.py .
COPY getLatestDataSets.py .
COPY read_covid_data.py .


# command to run on container start
CMD ["python","dash-covid-charts.py" ]