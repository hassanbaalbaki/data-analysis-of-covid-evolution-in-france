import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import streamlit as st
import read_covid_data
import getLatestDataSets

#Old Version using Streamlit use dash-covid-charts.py instead


#Title de la data app
st.title("Circulation du COVID-19 en FRANCE, par département")

def _max_width_():
    max_width_str = f"max-width: 2000px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>    
    """,
        unsafe_allow_html=True,
    )
def draw_curfew_dates():
    #Get the Date for curefews and confinement
    firstCurfew_date= read_covid_data.firstCurfew_date
    secondCurfew_date=read_covid_data.secondCurfew_date
    second_confinement_date= read_covid_data.second_confinement_date
    lite_confinement_date=read_covid_data.lite_confinement_date
    end_confinement_date= read_covid_data.end_confinement_date
    third_curfew_date=read_covid_data.third_curfew_date

    #draw the curefew and confinment periods
    plt.axvspan(firstCurfew_date, secondCurfew_date, facecolor='orange', alpha=0.1)
    plt.axvspan(secondCurfew_date,second_confinement_date, facecolor='orange', alpha=0.3)
    plt.axvspan(second_confinement_date,lite_confinement_date, facecolor='red', alpha=0.3)
    plt.axvspan(lite_confinement_date,end_confinement_date ,facecolor='red', alpha=0.1)
    plt.axvspan( end_confinement_date,third_curfew_date, facecolor='orange', alpha=0.3)
    plt.axvline(x=firstCurfew_date, label="Permier couvre-feu 10 métroples", color='orange',linestyle=':')
    plt.axvline(x=secondCurfew_date, label="Deuxième couvre-feu 54 départements", color='orange',linestyle='--')
    plt.axvline(x=second_confinement_date, label="Confinement", color='red',linestyle='--')
    plt.axvline(x=lite_confinement_date, label="Confinement Lite", color='red',linestyle='--')
    plt.axvline(x=end_confinement_date, label="Fin Confinement Lite", color='red',linestyle=':')
    plt.axvline(x=third_curfew_date, label="Couvre feu post confinement", color='orange',linestyle=':')
#Call max width to enlarge the charts
_max_width_()

if st.button("Mise à jour des données"):
    getLatestDataSets.update_covid_incidence_data()
    getLatestDataSets.update_PCR_data()

#Population data from : https://www.insee.fr/fr/statistiques/1893198
popEx = read_covid_data.get_population_df()

#données des test PCR issue de santé public
#PCR data from : https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-resultats-des-tests-virologiques-covid-19/
raw_tests = read_covid_data.get_pcr_df()

#hospitalisation (new Admissions) data from : https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/
raw_new_hosp=read_covid_data.get_daily_incidence_covid_metrics_df()

#hospitalisation (Current Admissions) data from : https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/
raw_current_hosp=read_covid_data.get_accumulated_covid_metrics_df()


list_all_deps= list(popEx.dep.to_numpy())

st.header("Choisir les départements à visionner:")
deps = st.multiselect("",list_all_deps,["06","13","38","42","69","75","83"])


#Get the data filtered for the selected departments
#PCR Test
tests_per_million= read_covid_data.get_pcr_tests_data_filtered_by_departement(deps)



#Chart Postifs Cases 
fig, ax = plt.subplots()
plt.rcParams["figure.figsize"] = [16,9]
#Positfs per dep  last 2 months
tests_per_million.loc[tests_per_million["jour"] > read_covid_data.firstCurfew_date ].plot(x="jour",y=deps,ax=ax, title= "Cas Positifs pour COVID (Incidence par million d'habitants)",grid=True)
#Dates of curfew and confinement
draw_curfew_dates()
#plt.show()
st.pyplot(fig)

#Get the data filtered for the selected departments
# incidence daily death and new hospital admission
new_hosp_per_million,new_death_per_million = read_covid_data.get_daily_incidence_metrics_filtered_by_departements(deps)

#New Hospital Admissions Charts 
#First and second wave 
fig, ax = plt.subplots()
new_hosp_per_million.plot(x="jour",y=deps,ax=ax, title= "Nouvelles admissions à l'hopital pour COVID (Incidence par million d'habitants)",grid=True)
#Dates of curfew and confinement
draw_curfew_dates()
#plt.show()
st.pyplot(fig)

#Second wave for New admissions data
fig, ax = plt.subplots()
new_hosp_per_million.loc[new_hosp_per_million["jour"] >= read_covid_data.firstCurfew_date].plot(x="jour",y=deps, ax=ax, title= "Nouvelles admissions à l'hopital pour COVID (Incidence par million d'habitants/ 2ème vague)",grid=True)
#Dates of curfew and confinement
draw_curfew_dates()
#plt.show()
st.pyplot(fig)

#Second wave for New death 
fig, ax = plt.subplots()
new_death_per_million.loc[new_death_per_million["jour"] >= read_covid_data.firstCurfew_date].plot(x="jour",y=deps, ax=ax, title= "Incidence de décés par jour à l'hopital pour COVID (Incidence par million d'habitants/ 2ème vague)",grid=True)
#Dates of curfew and confinement
draw_curfew_dates()
#plt.show()
st.pyplot(fig)



#Get the data filtered for the selected departments
# accumulated metrics for death , current hospitalisations and reanimations
cur_hosp_per_million,cur_rea_per_million,cur_dc_per_million,acc_dc_per_million_2nd_wave = read_covid_data.get_accumulated_metrics_filtered_by_departments(deps)


#Current Hospitalisations Charts 
#First and second wave 
fig, ax = plt.subplots()
cur_hosp_per_million.plot(x="jour",y=deps,ax=ax, title= "Nombre de patients à l'hopital pour COVID (Incidence par million d'habitants)",grid=True)
#Dates of curfew and confinement
draw_curfew_dates()
#plt.show()
st.pyplot(fig)

#Current Reanimation Patients Charts 
#First and second wave 
fig, ax = plt.subplots()
cur_rea_per_million.plot(x="jour",y=deps,ax=ax, title= "Nombre de patients en réanimation pour COVID (Incidence par million d'habitants)",grid=True)
#Dates of curfew and confinement
draw_curfew_dates()
#plt.show()
st.pyplot(fig)

#Current death Charts 
#First and second wave 
fig, ax = plt.subplots()
cur_dc_per_million.plot(x="jour",y=deps,ax=ax, title= "Nombre total de décés à l'hopital pour COVID (Incidence par million d'habitants)",grid=True)
#Dates of curfew and confinement
draw_curfew_dates()
#plt.show()
st.pyplot(fig)
#Second wave only
fig, ax = plt.subplots()
acc_dc_per_million_2nd_wave.plot(x="jour",y=deps,ax=ax, title= "Nombre accumulé de décés à l'hopital pour COVID (Incidence par million d'habitants) Seconde Vague Seulement",grid=True)
#Dates of curfew and confinement
draw_curfew_dates()
#plt.show()
st.pyplot(fig)


#After looping through the department plot the merged data
st.header("Cas Postifs (incidence par million d'habitants)")
st.write(tests_per_million.tail(10))
st.header("Nombre de patients hospitalisés pour Covid (incidence par million d'habitants)")
st.write(cur_hosp_per_million.tail(10))
st.header("Nombre de patients en réanimation (incidence par million d'habitants)")
st.write(cur_rea_per_million.tail(10))
st.header("Nouvelles Admissions à l'hôpital (incidence par million d'habitants)")
st.write(new_hosp_per_million.tail(10))
st.header("Décés(incidence par million d'habitants)")
st.write(cur_dc_per_million.tail(10))