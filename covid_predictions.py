import read_covid_data



import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import pandas as pd
import shutil
from sklearn.preprocessing import MinMaxScaler
from tqdm import tqdm_notebook as tqdm

from torch.utils.tensorboard import SummaryWriter
import matplotlib.pyplot as plt

from darts import TimeSeries
from darts.dataprocessing.transformers import Scaler
from darts.models import RNNModel, ExponentialSmoothing
from darts.metrics import mape
from darts.utils.statistics import check_seasonality, plot_acf

import warnings
warnings.filterwarnings("ignore")
import logging
logging.disable(logging.CRITICAL)
# Number of previous time stamps taken into account.
SEQ_LENGTH = 14
# Number of features in last hidden state
HIDDEN_SIZE = 25
# number of output time-steps to predict
OUTPUT_LEN = 1
# Number of stacked rnn layers.
NUM_LAYERS = 1


raw_tests = read_covid_data.get_pcr_df()
d = '06'

pcr_all_ages_06= raw_tests.loc[ (raw_tests["cl_age90"]==0) & (raw_tests["dep"]==d),["jour","P","T","positivity"] ]

if not pcr_all_ages_06.empty:
    #Smoothing over one week in order to remove weekend distortions
    pcr_all_ages_06["positifs"] = pcr_all_ages_06["P"].rolling(7).mean()   
else:
    print ( "No PCR  data in " + d)


plt.rcParams["figure.figsize"] = [16,9]

#Converting data frame to time series ( limited to the last 60 days)
pcr_all_ages_06_tseries = TimeSeries.from_dataframe(pcr_all_ages_06.tail(60), 'jour', 'positifs')

#Extracting training and validation sets
train, val = pcr_all_ages_06_tseries.split_before(pd.Timestamp('20210107'))

# Normalize the time series (note: we avoid fitting the transformer on the validation set)
transformer = Scaler()
train_transformed = transformer.fit_transform(train)
val_transformed = transformer.transform(val)
series_transformed = transformer.transform(pcr_all_ages_06_tseries)

covid_LSTM_model = RNNModel(
    model='LSTM',
    output_length=OUTPUT_LEN,
    hidden_size=HIDDEN_SIZE,
    n_rnn_layers=NUM_LAYERS,
    input_length=SEQ_LENGTH,
    dropout=0.4,
    batch_size=16,
    n_epochs=400,
    optimizer_kwargs={'lr': 1e-3},
    model_name='COVID_LSTM',
    log_tensorboard=True,
    random_state=42
)

covid_LSTM_model.fit(train_transformed, val_training_series=val_transformed, verbose=True)