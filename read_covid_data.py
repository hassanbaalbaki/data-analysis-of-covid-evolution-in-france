
import pandas as pd
import numpy as np

#datetime  convert based on https://gist.github.com/gjreda/7433f5f70299610d9b6b
from datetime import datetime
#convert from 2020-03-19
to_datetime = lambda d: datetime.strptime(d, '%Y-%m-%d')
#convert from 19/03/2020
to_frDate = lambda dt: datetime.strptime(dt,'%d/%m/%Y')

#Curfew and confinement dates from :
#  https://fr.wikipedia.org/wiki/Confinements_li%C3%A9s_%C3%A0_la_pand%C3%A9mie_de_Covid-19_en_France
#  https://fr.wikipedia.org/wiki/Liste_des_couvre-feu_de_2020-2021_en_France

firstCurfew_date=to_datetime("2020-10-16")
secondCurfew_date=to_datetime("2020-10-24")
second_confinement_date = to_datetime("2020-10-30")
lite_confinement_date = to_datetime("2020-11-28")
end_confinement_date = to_datetime("2020-12-15")
third_curfew_date = to_datetime("2021-01-02")#15 dep
forth_curfew_date = to_datetime("2021-01-12")#25 dep
generalised_curfew_date = to_datetime("2021-01-16")#France
weekend_confinement_date = to_datetime("2021-02-25")#Nice et Nord
possible_weekend_deconfinement_date = to_datetime("2021-03-15") #Nice

def get_population_df():
    #Population data from : https://www.insee.fr/fr/statistiques/1893198
    return pd.read_csv("data/population_2020.csv",sep=";")


#-----------------------------------PCR----------------------------------------------

def get_pcr_df():
    #données des test PCR issue de santé public
    #PCR data from : https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-resultats-des-tests-virologiques-covid-19/
    raw_tests = pd.read_csv("./data/tests_data.csv",sep=";",dtype={"dep":str,"P":int,"T":int}, converters={'jour': to_datetime})
    raw_tests["Cas Positifs"] = np.nan
    raw_tests["ppm"] = np.nan #ppm= Positif cases per million
    raw_tests["positivity"]= (100*raw_tests["P"])/raw_tests["T"]
    return raw_tests

#Sans classe d'ages , toutes ages confondues
def get_pcr_all_ages():
    pcr_all_ages_df= get_pcr_df()
    pcr_all_ages_df =pcr_all_ages_df.loc[ (pcr_all_ages_df["cl_age90"]==0) ,["jour","dep","P","T","Cas Positifs","ppm","positivity"] ]
    return pcr_all_ages_df




#PCR data all population normalised per mission
def get_pcr_tests_data_per_million():
    popEx = get_population_df()

    pcr_all_ages_normalised = get_pcr_all_ages()
    pcr_all_ages_normalised["diff_SP"]=np.nan # Weekly difference between the smoothed positif cases
    pcr_all_ages_normalised["pct_SP"]=np.nan # Percetange of weekly difference between the postifs cases
    pcr_all_ages_normalised["WP"]=np.nan # Sum of the number of positifs cases over the last 7 days
    pcr_all_ages_normalised["WT"]=np.nan # Sum of the number of tests done over the last 7 days
    pcr_all_ages_normalised["SP"]=np.nan # Smoothed Positfs cases
    pcr_all_ages_normalised["ST"]=np.nan # smmoothed Tests administered
    for index, dr in popEx.iterrows():
        d= dr["dep"]
        dep_pop = dr["population"]
        ratio_per_million= 1000000/int(dep_pop.replace(" ",""))
        #Smoothed over 1 week
        #lisser sur une semaine pour reduire l'effet du dimanche
        pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"SP"] = pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"P"].rolling(7).mean()
        pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"ST"] = pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"T"].rolling(7).mean()
        #Total of the last 7 days
        pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"WP"] = pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"P"].rolling(7).sum()
        pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"WT"] = pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"T"].rolling(7).sum()

        pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"diff_SP"] = pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"SP"].diff(7)

        pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"ppm"] = pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"SP"] * ratio_per_million
        pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"pct_SP"] = pcr_all_ages_normalised.loc[pcr_all_ages_normalised["dep"]==d,"SP"].pct_change(7)*100
    return pcr_all_ages_normalised

#PCR data all population normalised per mission, filtered by deps
def get_pcr_tests_data_per_million_filtered_by_departements(deps):
    pcr_per_million =  get_pcr_tests_data_per_million()
    return pcr_per_million.loc[pcr_per_million["dep"].isin(deps)]


#--------------------------------- Accumulated metrics or current occupation Levels ---------------------------

def get_accumulated_covid_metrics_df():
    #hospitalisation (Current Admissions) data from : https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/
    raw_current_hosp=pd.read_csv("./data/current_hosp_data.csv",sep=";",dtype={"dep":str,"sexe":int,"hosp":int,"rea":int,"rad":int,"dc":int},converters={"jour":to_datetime})
    raw_current_hosp["hpm"] =np.nan #hpm= Current hospitalisations per million
    raw_current_hosp["rpm"] =np.nan #rpm= Current intensive care per million
    raw_current_hosp["dpm"] =np.nan #dpm= Death at hospital per million

    raw_current_hosp["dc2"] =np.nan # Death since the Second wave (Autum 2020)
    raw_current_hosp["dpm2"] =np.nan #dpm= Death (since beginning of the second wave) at hospital per million

    return raw_current_hosp.loc[raw_current_hosp["sexe"]==0]

def get_accumulated_covid_metrics_per_million_df():
    popEx =get_population_df()
    acc_metrics_norm=get_accumulated_covid_metrics_df()
    for index, dr in popEx.iterrows():
        d= dr["dep"]
        dep_pop = dr["population"]
        ratio_per_million= 1000000/int(dep_pop.replace(" ",""))
        
        acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,"hpm"] = acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,"hosp"] * ratio_per_million
        acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,"rpm"] = acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,"rea"] * ratio_per_million
        acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,"dpm"] = acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,"dc"] * ratio_per_million
        #Second Wave
        
        total_dep_deaths_before_2nd_wave_sr = acc_metrics_norm.loc[((acc_metrics_norm["dep"] == d ) & (acc_metrics_norm["jour"] == firstCurfew_date)),"dc"]
        if total_dep_deaths_before_2nd_wave_sr.isnull(). values.any() :
            print("Second Wave starting value for the departement  " + d + " is null")
        else :

            total_dep_deaths_before_2nd_wave = total_dep_deaths_before_2nd_wave_sr.iloc[0]
            acc_metrics_norm.loc[(acc_metrics_norm["dep"]==d )& (acc_metrics_norm["jour"] >= firstCurfew_date  ),"dc2"] = acc_metrics_norm.loc[(acc_metrics_norm["dep"]==d )& (acc_metrics_norm["jour"] >= firstCurfew_date ) ,"dc"] - total_dep_deaths_before_2nd_wave
             #Death per million for the second wave
            acc_metrics_norm.loc[(acc_metrics_norm["dep"]==d) & (acc_metrics_norm["jour"]>= firstCurfew_date ),"dpm2"] = acc_metrics_norm.loc[ (acc_metrics_norm["dep"]==d) & (acc_metrics_norm["jour"]>= firstCurfew_date) ,"dc2"] * ratio_per_million
        #acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,["hmp","rmp","dpm"]] = acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,["hosp","rea","dc"]].to_numpy() * ratio_per_million
        #print("- Normalisation per million for " + dr["Nom_dep"] +"  #" + d + " population: "+ dr["population"])
        #print(acc_metrics_norm.loc[acc_metrics_norm["dep"]==d,["jour","hpm","rpm","dpm","hosp","rea","dc"]].tail(5))
    return acc_metrics_norm



def get_accumulated_covid_metrics_per_million_filtered_by_departements(deps):
    acc_metrics_norm=get_accumulated_covid_metrics_per_million_df()
    return acc_metrics_norm.loc[acc_metrics_norm["dep"].isin(deps)]


#------------------------------------------Hospital Incidence Admissions -------------------------------------

def get_daily_incidence_covid_metrics_df():
    #hospitalisation (new Admissions) data from : https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/
    raw_new_hosp=pd.read_csv("./data/new_hosp_data.csv",sep=";",dtype={"dep":str,"incid_hosp":int,"incid_rea":int,"incid_dc":int,"incid_rad":int},converters={"jour":to_datetime})
    
    raw_new_hosp["hpm"] =np.nan #hpm= New hospitalisations per million
    raw_new_hosp["dpm"] = np.nan # dpm = New Death per million 

    #Smoothing over one week in order to remove weekend distortions
    raw_new_hosp["hosp"] =np.nan # new column used for smoothing the hostpitalisation data 
    raw_new_hosp["death"] =np.nan # new column used for smoothing the death incidence data 

    return raw_new_hosp

#Admissions à l'hopital, lissé sur une semaine, ramené à une population de 1 million
def get_daily_incidence_covid_metrics_normalised_per_million():  

    normalized_daily_inc= get_daily_incidence_covid_metrics_df()
    popEx =get_population_df()
    for index, dr in popEx.iterrows():
        d= dr["dep"]
        dep_pop = dr["population"]
        ratio_per_million= 1000000/int(dep_pop.replace(" ",""))

        normalized_daily_inc.loc[normalized_daily_inc["dep"]==d,"hosp"]= normalized_daily_inc.loc[normalized_daily_inc["dep"]==d,"incid_hosp"].rolling(7).mean()# new column used for smoothing the hostpitalisation data
        normalized_daily_inc.loc[normalized_daily_inc["dep"]==d,"death"]= normalized_daily_inc.loc[normalized_daily_inc["dep"]==d,"incid_dc"].rolling(7).mean()# new column used for smoothing the death incidence data 

        #Normalise 
        normalized_daily_inc.loc[normalized_daily_inc["dep"]==d,"hpm"]= normalized_daily_inc.loc[normalized_daily_inc["dep"]==d,"hosp"] * ratio_per_million
        normalized_daily_inc.loc[normalized_daily_inc["dep"]==d,"dpm"]= normalized_daily_inc.loc[normalized_daily_inc["dep"]==d,"death"] * ratio_per_million
    
    return normalized_daily_inc

def get_daily_incidence_covid_metrics_normalised_per_million_filtered_by_departements(deps):
    normalized_daily_inc = get_daily_incidence_covid_metrics_normalised_per_million()
    return normalized_daily_inc.loc[normalized_daily_inc["dep"].isin(deps)]



