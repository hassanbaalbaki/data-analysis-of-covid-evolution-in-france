import requests
from bs4 import BeautifulSoup
import os

def check_data_folder():
    #check if the folder data exists , and create it otherwise
    if not os.path.exists("./data/"):
        print("Folder data was not Found")
        quit()

def update_covid_incidence_data():
    check_data_folder()
    # New hospital admissions for Covid-19
    URL_covid_hosp = 'https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/'
    page_hosp = requests.get(URL_covid_hosp)

    soup_hosp = BeautifulSoup(page_hosp.content, 'html.parser')

    soup_hosp_articles= soup_hosp.find_all('article')
    #New Admissions
    for article_elem in soup_hosp_articles:
        art_title= article_elem.find(class_='ellipsis')
        #print ("TITRE :" + art_title.text)
        if art_title.get_text().find("donnees-hospitalieres-nouveaux-covid19") !=-1:
            link_new_hosp= article_elem.find('a',href=True)
            print ("Downloading accumulated covid data --- >>: " + link_new_hosp['href'])
            new_hosp_request = requests.get(link_new_hosp['href'], allow_redirects=True)
            open('./data/new_hosp_data.csv', 'wb').write(new_hosp_request.content)
    #Total / Current Hospitalisations
    for article_elem in soup_hosp_articles:
        art_title= article_elem.find(class_='ellipsis')
        #print ("TITRE :" + art_title.text)
        if art_title.get_text().find("donnees-hospitalieres-covid19-") !=-1:
            link_total_hosp= article_elem.find('a',href=True)
            print ("Downloading daily covid incidence data --- >> : " + link_total_hosp['href'] + " aka. " +art_title.get_text() )
            total_hosp_request = requests.get(link_total_hosp['href'], allow_redirects=True)
            open('./data/current_hosp_data.csv', 'wb').write(total_hosp_request.content)
            break

def update_PCR_data():
    check_data_folder()
    #PCR Tests
    URL_covid_tests = 'https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-resultats-des-tests-virologiques-covid-19/'
    page_tests = requests.get(URL_covid_tests)

    soup_tests = BeautifulSoup(page_tests.content, 'html.parser')

    soup_tests_articles= soup_tests.find_all('article')
    for article_tests_elem in soup_tests_articles:
        art_title= article_tests_elem.find(class_='ellipsis')
        #print ("TITRE :" + art_title.text)
        if art_title.get_text().find("sp-pos-quot-dep-") !=-1:
            link_tests= article_tests_elem.find('a',href=True)
            print ("Downloading latest PCR data --- >> : " + link_tests['href'])
            tests_request = requests.get(link_tests['href'], allow_redirects=True)
            open('./data/tests_data.csv', 'wb').write(tests_request.content)
